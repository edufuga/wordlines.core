package com.gitlab.edufuga.wordlines.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

public class WordStatusDate implements Comparable<WordStatusDate> {
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    private final String word;
    private final Status status;
    private final Date date;

    public WordStatusDate(String word, Status status, String date) throws ParseException {
        this.word = word;
        this.status = status;
        this.date = dateFormat.parse(date);
    }

    public WordStatusDate(String word, Status status, Date date) {
        this.word = word;
        this.status = status;
        this.date = date;
    }

    public WordStatusDate(String word, String status, String date) throws ParseException {
        this.word = word;
        this.date = dateFormat.parse(date);
        this.status = (status.length() == 1)
                ? Arrays.stream(Status.values()).filter(it -> it.name().startsWith(status.toUpperCase())).findAny().get()
                : Status.valueOf(status);
    }

    public String toString() {
        return getWord() + "\t" + getStatus() + "\t" + WordStatusDate.getDateFormat().format(getDate());
    }

    public String getWord() {
        return word;
    }

    public Status getStatus() {
        return status;
    }

    public Date getDate() {
        return date;
    }

    public static SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    @Override
    public int compareTo(WordStatusDate that) {
        return Comparator.comparing(WordStatusDate::getWord)
                .thenComparing(WordStatusDate::getStatus)
                .thenComparing(WordStatusDate::getDate)
                .compare(this, that);
    }
}
