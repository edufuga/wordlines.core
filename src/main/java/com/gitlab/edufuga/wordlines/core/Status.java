package com.gitlab.edufuga.wordlines.core;

import java.util.stream.Stream;

public enum Status {
    NEW, IGNORED, UNKNOWN, GUESSED, COMPREHENDED, KNOWN, WELL_KNOWN, MASTERED;

    public static Status getFor(Character entered) {
        return Stream.of(values())
                .filter((Status it) -> it.name().toLowerCase().startsWith(""+entered))
                .findAny()
                .get();
    }
}
